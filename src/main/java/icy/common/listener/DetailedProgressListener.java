package icy.common.listener;

/**
 * Progress notification listener enriched with optional comments and optional data.
 * 
 * @author Daniel Felipe Gonzalez Obando
 */
public interface DetailedProgressListener {
	public boolean notifyProgress(double position, String comment, Object data);
}
